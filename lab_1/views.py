from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Feril Bagus Perkasa' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 7, 8) #TODO Implement this, format (Year, Month, Date)
npm = 1706075054 # TODO Implement this
college = "Universitas Indonesia"
born_place = "Bogor"
born_date = "08 - 07 - 1999"
hobby = "Bermain Game"

mhs_name_1 = 'Ringgi Cahyo Dwiputra' # TODO Implement this
birth_date_1 = date(1999, 2, 25) #TODO Implement this, format (Year, Month, Date)
npm_1 = 1706025005 # TODO Implement this
college = "Universitas Indonesia"
born_place_1 = "Jakarta"
born_date_1 = "25 - 02 - 1999"
hobby_1 = "Ngoding"

mhs_name_2 = 'Muhammad Abdurahman' # TODO Implement this
birth_date_2 = date(1999, 4, 14) #TODO Implement this, format (Year, Month, Date)
npm_2 = 170639925 # TODO Implement this
college = "Universitas Indonesia"
born_place_2 = "Tanggerang"
born_date_2 = "14 - 04 - 1999"
hobby_2 = "Tidur"

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
                'college' : college, 'born_place' : born_place, 'born_date' : born_date,
                'hobby' : hobby,
                
                'name_1': mhs_name_1, 'age_1': calculate_age(birth_date_1.year), 'npm_1': npm_1,
                'college' : college, 'born_place_1' : born_place_1, 'born_date_1' : born_date_1,
                'hobby_1' : hobby,
                
                'name_2': mhs_name_2, 'age_2': calculate_age(birth_date_2.year), 'npm_2': npm_2,
                'college' : college, 'born_place_2' : born_place_2, 'born_date_2' : born_date_2,
                'hobby_2' : hobby_2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
