from django.urls import re_path
from .views import index, jadwal_post, jadwal_table, delete_data
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^jadwal_post', jadwal_post, name='jadwal_post'),
    re_path(r'^jadwal_table', jadwal_table, name='jadwal_table'),
    re_path(r'^delete_data', delete_data, name='delete_data'),
]
