from django import forms

class Jadwal_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Nama Kegiatan', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='Tempat', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Kategori Kegiatan', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    time = forms.CharField(label= 'Tanggal dan Waktu',required=True, widget= forms.DateTimeInput(attrs={'class': 'form-control','id': 'date_time'}))
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True)
