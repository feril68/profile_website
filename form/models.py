from django.db import models

# Create your models here.
class Jadwal(models.Model):
    name = models.CharField(max_length=27)
    place = models.CharField(max_length=27)
    category = models.CharField(max_length=27)
    time = models.DateTimeField()
    message = models.CharField(max_length=27)

    def __str__(self):
        return self.message
